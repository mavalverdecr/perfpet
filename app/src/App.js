import React from 'react';
import { Navbar, Container, Nav } from 'react-bootstrap';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

//Components
import ListPets from './components/ListPets/ListPets';
import Pet from './components/Pets/Pet';

const App = () => {
  return (
    <div>
      <Navbar bg="dark" variant="dark">
          <Container>
              <Navbar.Brand>PerfPet</Navbar.Brand>
              <Nav className="me-auto">
                <Nav.Link href="/" style={{cursor:'pointer'}}>Home</Nav.Link>
              </Nav>
          </Container>
      </Navbar>
      <Router>
          <Routes>
              <Route 
                  exact path="/"
                  element={<ListPets />}
              />
              <Route 
                  path="/:id"
                  element={<Pet />}
              />
          </Routes>
      </Router>
    </div>
  )
}

export default App;
