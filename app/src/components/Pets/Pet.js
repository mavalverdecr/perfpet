import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import { Container, Card, Row, Col } from 'react-bootstrap';

import './estilos.css'
import heartFill from '../../images/heart-fill.png'
import heartEmpty from '../../images/heart-empty.png'

const Pet = () => {
    
    //State
    const [pet, setPet] = useState({});
    const {id} = useParams();    
    
    //Functions
    const getPet = async () => {
        try {
            const res = await axios.get(
                process.env.REACT_APP_API_URL+'/pets/'+id,
            ) 
            setPet(res.data)

        } catch (err) {
            console.log(err)
        }
    }

    const setLike = async e => {
        const id = e.target.name
        try {
            const res = await axios.patch(process.env.REACT_APP_API_URL+'/pets/'+id) 
            setPet(pet.id === res.data.id ? res.data : pet)
        } catch (err) {
            console.log(err)
        }
    }

    useEffect(() => {
        getPet()
    },[])

    return (
        <Container>
            <Row>
                <Col xs={3}></Col>
                <Col xs={6}>
                    <Card style={{ width: '70%' }}>
                        <Card.Img variant="top" src={pet.img}/>
                        <Card.Body>
                            <Card.Title>{pet.name}</Card.Title>
                            <Card.Text style={{height: '80px'}}>{pet.description}</Card.Text>
                            <img className="like" name={pet.id} onClick={setLike} src={pet.like === undefined || pet.like === false ? heartEmpty : heartFill} alt="like" />
                        </Card.Body>
                    </Card>
                </Col>
                <Col xs={3}></Col>
            </Row>
        </Container>
    )
}

export default Pet;