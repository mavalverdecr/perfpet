import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

import { Container, Row, Col, Card } from 'react-bootstrap';
import './estilos.css'
import heartFill from '../../images/heart-fill.png'
import heartEmpty from '../../images/heart-empty.png'
import detail from '../../images/detail.png'

const ListPets = () => {

    //State
    const [pets, setPets] = useState([]);

    //Functions
    const getPets = async () => {
        try {
            const res = await axios.get(process.env.REACT_APP_API_URL+'/pets') 
            setPets(res.data)
        } catch (err) {
            console.log(err)
        }
    }

    const setLike = async e => {
        const id = e.target.name
        try {
            const res = await axios.patch(process.env.REACT_APP_API_URL+'/pets/'+id) 
            setPets(pets.map( pet => (pet.id === res.data.id ? res.data : pet)))
        } catch (err) {
            console.log(err)
        }
    }

    useEffect(() => {
        getPets()
    },[])

    return (
        <Container>
            <Row>
                {pets.map(pet => (
                    <Col key={pet.id}>   
                        <Card style={{ width: '100%' }}>
                            <Card.Img variant="top" src={pet.img} />
                            <Card.Body>
                                <Card.Title>{pet.name}</Card.Title>
                                <Card.Text style={{height: '80px'}}>{pet.description}</Card.Text>
                                <img className="like" name={pet.id} onClick={setLike} src={pet.like === undefined || pet.like === false ? heartEmpty : heartFill} alt="like"/>
                                <Link to={"/" + pet.id}>
                                <img className="details" src={detail} alt="details"/>
                                </Link>
                            </Card.Body>
                        </Card>
                    </Col> 
                ))}                
            </Row>
        </Container>
    )    
}

export default ListPets;