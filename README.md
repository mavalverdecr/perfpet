# PerfPet

_Web App desarrollada con ReactJS y NodeJs para escoger tu mascota favorita._

![PerfPet](./perfpet_home.png)

## Instalación :wrench:

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

### Docker

_Instalar [Docker](https://docs.docker.com/engine/install/) en tu equipo._

_Desde la terminal, levantar los contenedores del API y el front simultaneamente._

```
docker compose up -d
```

_Desde el navegador, acceder a la URL del proyecto [http://localhost:3000]._

## Construido con :pencil:

* [NodeJS](https://nodejs.org/) - Backend
* [Firebase - Firestore](https://firebase.google.com/) - Base de datos
* [ReactJs](https://reactjs.org/) - Frontend
* [Bootstrap 5](https://getbootstrap.com/) - Estilos CSS

## Autor :alien:

* **Manu Valverde** - *Trabajo Inicial* - [Github](https://github.com/mavalverdecr)

## Licencia :scroll:

Este proyecto está bajo la Licencia (MIT) - mira el archivo [LICENSE.md](LICENSE.md) para detalles

---
⌨️ con ❤️ por [Manu Valverde](https://github.com/mavalverdecr) 😊