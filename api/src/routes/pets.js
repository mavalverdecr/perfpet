const express = require('express');
const router = express.Router();
const {db} = require('../middlewares/firebase');

router.get('/', async (req, res) => {
    const querySnapshot = await db.collection('pets').get();
    const pets = querySnapshot.docs.map(doc => ({
        id: doc.id,
        ...doc.data()
    }))
    res.send(pets)
});

router.get('/:id', async (req, res) => {
    const id = req.params.id;
    const doc = await db.collection('pets').doc(id).get();
    const pet = {
        id: doc.id,
        ...doc.data()
    }
    res.send(pet)
});

router.patch('/:id', async (req, res) => {
    const id = req.params.id;
    const doc = db.collection('pets').doc(id);
    const get = await doc.get();
    const { like } = get.data();

    const response = await doc.set({
        ...get.data(),
        like: like === undefined ? true : !like
    })

    const pet = {
        id: doc.id,
        ...get.data(),
        like: !like
    }
    res.send(pet)
});

module.exports = router;