const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const app = express();
//Import Routes
const pets = require('./routes/pets');

//Middlewares
app.use(express.json());
app.use(cors());
app.use(morgan('dev'));

//Routes
app.use('/pets',pets);

//Starting server
app.listen(3001, () => { console.log('Server is running!') } );